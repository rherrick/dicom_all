XNAT DICOM Aggregator Library (dicom-all)
================================

The XNAT DICOM aggregator library provides an easy way to build and deploy all of the
XNAT DICOM management libraries in a single operation. This aggregates all of the 
upstream modules into a single build list.

Contents
--------

The referenced libraries are:

-   ExtAttr (https://bitbucket.org/nrg/extattr)
-   DICOM Utilities (https://bitbucket.org/nrg/dicomutils)
-   DICOM Image Utilities (https://bitbucket.org/nrg/dicomimageutils)
-   Dicom DB (https://bitbucket.org/nrg/dicomdb)
-   DicomEdit (https://bitbucket.org/nrg/dicomedit)
-   Session Builders (https://bitbucket.org/nrg/sessionbuilders)
-   Ecat4XNAT (https://bitbucket.org/nrg/ecat4xnat)
-   PrearcImporter (https://bitbucket.org/nrg/prearcimporter)
-   DICOM XNAT (https://bitbucket.org/nrg/dicom-xnat)

Building
--------

To build all of these modules, invoke Maven with the desired lifecycle phase.
For example, the following command will clean previous builds then build new 
jar files, along with archives containing each library's source code and JavaDocs,
run each library's unit tests, and install each jar into the local repository:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mvn clean install
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
